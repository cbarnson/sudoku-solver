# Sudoku Puzzle Solver

Solves sudoku puzzles!

## Installation

Download the source code and compile with <make>

## Usage

To enter a (solvable) puzzle, replace the sample puzzle in src/input with the to-be-solved
puzzle, using the format as shown (0 in place of empty squares).  Compile and run the 
created executable

## Credits

Written by Cody Barnson
